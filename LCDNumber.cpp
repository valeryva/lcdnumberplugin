#include "LCDNumber_p.h"
#include "LCDNumber.h"
#include <QQuickPaintedItem>

extern QString double2string(double num, int base, int ndigits, bool *oflow);
extern QString int2string(int num, int base, int ndigits, bool *oflow);

/*!
    Constructs an LCD number, sets the number of digits to 5, the base
    to decimal, the decimal point mode to 'small' and the frame style
    to a raised box. The segmentStyle() is set to \c Outline.

    The \a parent argument is passed to the QFrame constructor.

    \sa setDigitCount(), setSmallDecimalPoint()
*/

LCDNumber::LCDNumber(QQuickItem *parent)
    : LCDNumber(5, parent)
{
}

/*!
    Constructs an LCD number, sets the number of digits to \a
    numDigits, the base to decimal, the decimal point mode to 'small'
    and the frame style to a raised box. The segmentStyle() is set to
    \c Filled.

    The \a parent argument is passed to the QFrame constructor.

    \sa setDigitCount(), setSmallDecimalPoint()
(QQuickItem *parent)
    : QQuickPaintedItem(parent)
*/

LCDNumber::LCDNumber(uint numDigits, QQuickItem *parent)
    : QQuickPaintedItem(parent)
    , m_text()
    , d_ptr(new LCDNumberPrivate(this))
{
    Q_D(LCDNumber);
    d->ndigits = numDigits;
    d->init();
}
/*
void QLCDNumber::paintEvent(QPaintEvent *)
{
    Q_D(LCDNumber);
    QPainter p(this);
    drawFrame(&p);
    p.setRenderHint(QPainter::Antialiasing);
    if (d->shadow)
        p.translate(0.5, 0.5);

    if (d->smallPoint)
        d->drawString(d->digitStr, p, &d->points, false);
    else
        d->drawString(d->digitStr, p, 0, false);
}
*/

void LCDNumber::paint(QPainter *painter)
{
    if (m_text.isEmpty())
    {
        return;
    }

    Q_D(LCDNumber);
    painter->setRenderHint(QPainter::Antialiasing);
    if (d->shadow)
        painter->translate(0.5, 0.5);

    d->digitStr = m_text;

    if (d->smallPoint)
        d->drawString(d->digitStr, *painter, &d->points, false);
    else
        d->drawString(d->digitStr, *painter, 0, false);
}

/*!
    \since 4.6
    \property QLCDNumber::digitCount
    \brief the current number of digits displayed

    Corresponds to the current number of digits. If \l
    QLCDNumber::smallDecimalPoint is false, the decimal point occupies
    one digit position.

    By default, this property contains a value of 5.

    \sa smallDecimalPoint
*/

/*!
  Sets the current number of digits to \a numDigits. Must
  be in the range 0..99.
 */
void LCDNumber::setDigitCount(int numDigits)
{
    Q_D(LCDNumber);
    if (Q_UNLIKELY(numDigits > 99)) {
        qWarning("QLCDNumber::setNumDigits: (%s) Max 99 digits allowed",
                 objectName().toLocal8Bit().constData());
        numDigits = 99;
    }
    if (Q_UNLIKELY(numDigits < 0)) {
        qWarning("QLCDNumber::setNumDigits: (%s) Min 0 digits allowed",
                 objectName().toLocal8Bit().constData());
        numDigits = 0;
    }
    if (d->digitStr.isNull()) {                  // from constructor
        d->ndigits = numDigits;
        d->digitStr.fill(QLatin1Char(' '), d->ndigits);
        d->points.fill(0, d->ndigits);
        d->digitStr[d->ndigits - 1] = QLatin1Char('0'); // "0" is the default number
    } else {
        bool doDisplay = d->ndigits == 0;
        if (numDigits == d->ndigits)             // no change
            return;
        int i;
        int dif;
        if (numDigits > d->ndigits) {            // expand
            dif = numDigits - d->ndigits;
            QString buf;
            buf.fill(QLatin1Char(' '), dif);
            d->digitStr.insert(0, buf);
            d->points.resize(numDigits);
            for (i=numDigits-1; i>=dif; i--)
                d->points.setBit(i, d->points.testBit(i-dif));
            for (i=0; i<dif; i++)
                d->points.clearBit(i);
        } else {                                        // shrink
            dif = d->ndigits - numDigits;
            d->digitStr = d->digitStr.right(numDigits);
            QBitArray tmpPoints = d->points;
            d->points.resize(numDigits);
            for (i=0; i<(int)numDigits; i++)
                d->points.setBit(i, tmpPoints.testBit(i+dif));
        }
        d->ndigits = numDigits;
        if (doDisplay)
            display(value());
        update();
    }
}

/*!
  Returns the current number of digits.
 */
int LCDNumber::digitCount() const
{
    Q_D(const LCDNumber);
    return d->ndigits;
}

/*!
    \overload

    Returns \c true if \a num is too big to be displayed in its entirety;
    otherwise returns \c false.

    \sa display(), digitCount(), smallDecimalPoint()
*/

bool LCDNumber::checkOverflow(int num) const
{
    Q_D(const LCDNumber);
    bool of;
    int2string(num, d->base, d->ndigits, &of);
    return of;
}


/*!
    Returns \c true if \a num is too big to be displayed in its entirety;
    otherwise returns \c false.

    \sa display(), digitCount(), smallDecimalPoint()
*/

bool LCDNumber::checkOverflow(double num) const
{
    Q_D(const LCDNumber);
    bool of;
    double2string(num, d->base, d->ndigits, &of);
    return of;
}


/*!
    \property QLCDNumber::mode
    \brief the current display mode (number base)

    Corresponds to the current display mode, which is one of \c Bin,
    \c Oct, \c Dec (the default) and \c Hex. \c Dec mode can display
    floating point values, the other modes display the integer
    equivalent.

    \sa smallDecimalPoint(), setHexMode(), setDecMode(), setOctMode(), setBinMode()
*/

LCDNumber::Mode LCDNumber::mode() const
{
    Q_D(const LCDNumber);
    return (LCDNumber::Mode) d->base;
}

void LCDNumber::setMode(Mode m)
{
    Q_D(LCDNumber);
    d->base = m;
    display(d->val);
}

/*!
    \property QLCDNumber::value
    \brief the displayed value

    This property corresponds to the current value displayed by the
    LCDNumber.

    If the displayed value is not a number, the property has a value
    of 0.

    By default, this property contains a value of 0.
*/

double LCDNumber::value() const
{
    Q_D(const LCDNumber);
    return d->val;
}

/*!
    Displays the number represented by the string \a s.

    This version of the function disregards mode() and
    smallDecimalPoint().

    These digits and other symbols can be shown: 0/O, 1, 2, 3, 4, 5/S,
    6, 7, 8, 9/g, minus, decimal point, A, B, C, D, E, F, h, H, L, o,
    P, r, u, U, Y, colon, degree sign (which is specified as single
    quote in the string) and space. QLCDNumber substitutes spaces for
    illegal characters.
*/

void LCDNumber::display(const QString &s)
{
    Q_D(LCDNumber);
    d->val = 0;
    bool ok = false;
    double v = s.toDouble(&ok);
    if (ok)
        d->val = v;
    d->internalSetString(s);
}

/*!
    \overload

    Displays the number \a num.
*/
void LCDNumber::display(double num)
{
    Q_D(LCDNumber);
    d->val = num;
    bool of;
    QString s = double2string(d->val, d->base, d->ndigits, &of);
    if (of)
        emit overflow();
    else
        d->internalSetString(s);
}

/*!
    \overload

    Displays the number \a num.
*/
void LCDNumber::display(int num)
{
    Q_D(LCDNumber);
    d->val = (double)num;
    bool of;
    QString s = int2string(num, d->base, d->ndigits, &of);
    if (of)
        emit overflow();
    else
        d->internalSetString(s);
}

/*!
    \property QLCDNumber::segmentStyle
    \brief the style of the LCDNumber

    \table
    \header \li Style \li Result
    \row \li \c Outline
         \li Produces raised segments filled with the background color
    \row \li \c Filled
            (this is the default).
         \li Produces raised segments filled with the foreground color.
    \row \li \c Flat
         \li Produces flat segments filled with the foreground color.
    \endtable

    \c Outline and \c Filled will additionally use
    QPalette::light() and QPalette::dark() for shadow effects.
*/
void LCDNumber::setSegmentStyle(SegmentStyle s)
{
    Q_D(LCDNumber);
    d->fill = (s == Flat || s == Filled);
    d->shadow = (s == Outline || s == Filled);
    update();
}

LCDNumber::SegmentStyle LCDNumber::segmentStyle() const
{
    Q_D(const LCDNumber);
    Q_ASSERT(d->fill || d->shadow);
    if (!d->fill && d->shadow)
        return Outline;
    if (d->fill && d->shadow)
        return Filled;
    return Flat;
}

/*!\reimp
*/
/*
QSize LCDNumber::sizeHint() const
{
    return QSize(10 + 9 * (digitCount() + (smallDecimalPoint() ? 0 : 1)), 23);
}
*/
/*!
    Calls setMode(Hex). Provided for convenience (e.g. for
    connecting buttons to it).

    \sa setMode(), setDecMode(), setOctMode(), setBinMode(), mode()
*/

void LCDNumber::setHexMode()
{
    setMode(Hex);
}

/*!
    Calls setMode(Dec). Provided for convenience (e.g. for
    connecting buttons to it).

    \sa setMode(), setHexMode(), setOctMode(), setBinMode(), mode()
*/

void LCDNumber::setDecMode()
{
    setMode(Dec);
}

/*!
    Calls setMode(Oct). Provided for convenience (e.g. for
    connecting buttons to it).

    \sa setMode(), setHexMode(), setDecMode(), setBinMode(), mode()
*/

void LCDNumber::setOctMode()
{
    setMode(Oct);
}


/*!
    Calls setMode(Bin). Provided for convenience (e.g. for
    connecting buttons to it).

    \sa setMode(), setHexMode(), setDecMode(), setOctMode(), mode()
*/

void LCDNumber::setBinMode()
{
    setMode(Bin);
}


/*!
    \property QLCDNumber::smallDecimalPoint
    \brief the style of the decimal point

    If true the decimal point is drawn between two digit positions.
    Otherwise it occupies a digit position of its own, i.e. is drawn
    in a digit position. The default is false.

    The inter-digit space is made slightly wider when the decimal
    point is drawn between the digits.

    \sa mode
*/

void LCDNumber::setSmallDecimalPoint(bool b)
{
    Q_D(LCDNumber);
    d->smallPoint = b;
    update();
}

bool LCDNumber::smallDecimalPoint() const
{
    Q_D(const LCDNumber);
    return d->smallPoint;
}

void LCDNumber::setTextColor(QColor t)
{
    Q_D(LCDNumber);
    d->m_fgColor = t;
}

QColor LCDNumber::textColor() const
{
    Q_D(const LCDNumber);
    return d->m_fgColor;
}

void LCDNumber::setDarkColor(QColor t)
{
    Q_D(LCDNumber);
    d->m_darkColor = t;
}

QColor LCDNumber::darkColor() const
{
    Q_D(const LCDNumber);
    return d->m_darkColor;
}

void LCDNumber::setLightColor(QColor t)
{
    Q_D(LCDNumber);
    d->m_lightColor = t;
}

QColor LCDNumber::lightColor() const
{
    Q_D(const LCDNumber);
    return d->m_lightColor;
}

/*!
    \property QLCDNumber::intValue
    \brief the displayed value rounded to the nearest integer

    This property corresponds to the nearest integer to the current
    value displayed by the LCDNumber. This is the value used for
    hexadecimal, octal and binary modes.

    If the displayed value is not a number, the property has a value
    of 0.

    By default, this property contains a value of 0.
*/
int LCDNumber::intValue() const
{
    Q_D(const LCDNumber);
    return qRound(d->val);
}
