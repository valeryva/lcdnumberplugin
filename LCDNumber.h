#ifndef LCDNUMBER_H
#define LCDNUMBER_H

#include <QtQuick>

class LCDNumberPrivate;

class LCDNumber : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged)
    Q_PROPERTY(bool smallDecimalPoint READ smallDecimalPoint WRITE setSmallDecimalPoint)
    Q_PROPERTY(int digitCount READ digitCount WRITE setDigitCount)
    Q_PROPERTY(Mode mode READ mode WRITE setMode)
    Q_PROPERTY(SegmentStyle segmentStyle READ segmentStyle WRITE setSegmentStyle)
    Q_PROPERTY(double value READ value WRITE display)
    Q_PROPERTY(int intValue READ intValue WRITE display)

    Q_PROPERTY(QColor lightColor READ lightColor WRITE setLightColor)
    Q_PROPERTY(QColor darkColor  READ darkColor  WRITE setDarkColor)
    Q_PROPERTY(QColor textColor  READ textColor  WRITE setTextColor)

private:
    Q_DECLARE_PRIVATE(LCDNumber)

public:

    LCDNumber(QQuickItem* parent = nullptr);
    LCDNumber(uint numDigits, QQuickItem *parent = nullptr);
    virtual ~LCDNumber() {}

    enum Mode {
        Hex, Dec, Oct, Bin
    };

    Q_ENUM(Mode)

    enum SegmentStyle {
        Outline, Filled, Flat
    };

    Q_ENUM(SegmentStyle)

    void paint( QPainter* painter );

    inline QString getText()            { return m_text; }
    inline void setText(QString t)      { m_text = t; }

    bool smallDecimalPoint() const;
    int digitCount() const;
    void setDigitCount(int nDigits);

    bool checkOverflow(double num) const;
    bool checkOverflow(int num) const;

    Mode mode() const;
    void setMode(Mode);

    SegmentStyle segmentStyle() const;
    void setSegmentStyle(SegmentStyle);

    double value() const;
    int intValue() const;

    void setTextColor(QColor t);
    QColor textColor() const;
    void setDarkColor(QColor t);
    QColor darkColor() const;
    void setLightColor(QColor t);
    QColor lightColor() const;

public slots:

    void display(const QString &str);
    void display(int num);
    void display(double num);
    void setHexMode();
    void setDecMode();
    void setOctMode();
    void setBinMode();
    void setSmallDecimalPoint(bool);
    //QSize sizeHint() const;

signals:
    void textChanged();
    void overflow();

private:
    QString m_text;

private:
     LCDNumberPrivate* d_ptr;
};

#endif // LCDNUMBER_H
