TEMPLATE = lib
CONFIG += qt plugin c++11
QT += qml quick

TARGET = lcdnumberplugin

HEADERS += \
    LCDNumber.h \
    LCDNumber_p.h \
    Plugin/plugin.h

SOURCES += \
        LCDNumber.cpp \
        LCDNumber_p.cpp

OTHER_FILES += qmldir

DESTDIR = LCDNumberPlugin
DESTPATH = $$[QT_INSTALL_QML]/$$DESTDIR
target.path = $$DESTPATH
qmldir.files = $$PWD/Plugin/qmldir
qmldir.path += $$DESTPATH

INSTALLS += qmldir target
CONFIG += install_ok
