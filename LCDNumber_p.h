#ifndef LCDNUMBERPRIVATE_H
#define LCDNUMBERPRIVATE_H

#include <QObject>
#include "LCDNumber.h"

class LCDNumberPrivate : public QObject
{
    Q_DECLARE_PUBLIC(LCDNumber)

public:

    LCDNumberPrivate(LCDNumber* q = nullptr);

    void init();
    void internalSetString(const QString& s);
    void drawString(const QString& s, QPainter &p, QBitArray* a = nullptr, bool = true);
    //void drawString(const QString &, QPainter &, QBitArray * = 0) const;
    void drawDigit(const QPoint &, QPainter &, int, char, char = ' ');
    void drawSegment(const QPoint &, char, QPainter &, int, bool = false);

    int ndigits;
    double val;
    uint base : 2;
    uint smallPoint : 1;
    uint fill : 1;
    uint shadow : 1;
    QString digitStr;
    QBitArray points;

private:
    LCDNumber* q_ptr;
    QColor m_lightColor = "white";
    QColor m_darkColor = "darkblue";
    QColor m_fgColor = "green";
};

#endif // LCDNUMBERPRIVATE_H
