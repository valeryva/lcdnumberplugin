#include <QQmlExtensionPlugin>

#include "../LCDNumber.h"

class LCDNumberPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)
public:
    void registerTypes(const char *uri) override
    {
        qmlRegisterType<LCDNumber>(uri, 1, 0, "LCDNumber");
    }
};
