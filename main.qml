import QtQuick 2.12
import QtQuick.Controls 2.12
import LCDNumberPlugin 1.0

ApplicationWindow {
    id: mainWin
    visible: true
    width: 640
    height: 480
    title: qsTr("LCDNumber Test")

    Column {
        spacing: 40
        anchors.fill: parent

        LCDNumber {
            id: lcd1
            width: parent.width
            height: parent.height * 0.3
            text: "00:00:00"
            digitCount: 8
            textColor: "red"
        }

        LCDNumber {
            id: lcd2
            width: parent.width
            height: parent.height * 0.2
            text: "00:00:00"
            digitCount: 8
            textColor: "blue"
            smallDecimalPoint: true
        }

        LCDNumber {
            id: lcd3
            width: parent.width
            height: parent.height * 0.1
            text: "00:00:00"
            digitCount: 8
            textColor: "green"
            smallDecimalPoint: true
        }
    }

    Timer {
        interval: 1000; running: true; repeat: true
        onTriggered: {
            lcd1.text = Qt.formatTime(new Date(),"hh:mm:ss")
            lcd2.text = lcd1.text
            lcd3.text = lcd1.text
            lcd1.update()
            lcd2.update()
            lcd3.update()
        }
    }
}
