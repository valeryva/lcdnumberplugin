QT += quick qml
CONFIG += c++11 release

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp

RESOURCES += qml.qrc

# for debug only
QML_IMPORT_TRACE = 1

# Additional import path used to resolve QML modules in Qt Creator's code model
QML2_IMPORT_PATH = $$[QT_INSTALL_QML]

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

